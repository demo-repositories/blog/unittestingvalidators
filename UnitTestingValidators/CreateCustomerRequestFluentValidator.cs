using FluentValidation;

namespace UnitTestingValidators;

public class CreateCustomerRequestFluentValidator : AbstractValidator<CreateCustomerRequest>
{
    public CreateCustomerRequestFluentValidator()
    {
        RuleFor(x => x.Name)
            .NotEmpty();

        RuleFor(x => x.Age)
            .GreaterThanOrEqualTo(18);
    }
}