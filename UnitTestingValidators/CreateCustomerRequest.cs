namespace UnitTestingValidators;

public class CreateCustomerRequest
{
    public string Name { get; set; }
    public int Age { get; set; }
}