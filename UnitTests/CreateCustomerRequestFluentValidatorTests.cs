using System;
using FluentValidation;
using UnitTestingValidators;
using Xunit;

namespace UnitTests;

public class CreateCustomerRequestFluentValidatorTests : ValidatorTestBase<CreateCustomerRequest>
{
    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    public void Name_cannot_be_empty(string name)
    {
        // Arrange
        Action<CreateCustomerRequest> mutation = x => x.Name = name; 

        // Act
        var result = Validate(mutation);

        // Assert
        result.ShouldHaveValidationErrorFor(x => x.Name);
    }
    
    [Theory]
    [InlineData(17)]
    [InlineData(15)]
    [InlineData(0)]
    [InlineData(-3)]
    public void Age_cannot_be_less_than_18(int age)
    {
        // Arrange
        Action<CreateCustomerRequest> mutation = x => x.Age = age;

        // Act
        var result = Validate(mutation);

        // Assert
        result.ShouldHaveValidationErrorFor(x => x.Age);
    }

    protected override CreateCustomerRequest CreateValidObject()
    {
        return new CreateCustomerRequest
        {
            Name = "John Doe",
            Age = 20,
        };
    }

    protected override IValidator<CreateCustomerRequest> CreateValidator()
    {
        return new CreateCustomerRequestFluentValidator();
    }
}